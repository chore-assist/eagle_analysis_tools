import sys
from eagle_sb_generator import *


def sb_profile_generator ( snapshot, n_threads, SF_number_density_thr ):
    esb = EagleSBGenerator ()

    print ( "Cutting halos..." )
    esb.p2c (
        './../chombo/P2C/bin/P2C-1.2',
        snapshot,
        output_prefix='./cutted_halos/',
        n_threads=n_threads
    )

    print ( "Ray tracing...")
    esb.radamesh (
        './../init_evol/Radamesh/bin/Radamesh-1.3_64',
        output_prefix='./radamesh_output/',
        n_threads=n_threads
    )

    print ( "Surface brightness maps...")
    esb.surface_brightness (
        SF_number_density_thr=SF_number_density_thr,
        output_prefix='./sb_output/',
        n_threads=n_threads
    )


if __name__ == "__main__":
    if len(sys.argv) != 4:
       print (
           "Wrong number of arguments:",
           " python3 ./surface_brightness_generator.py <snapshot> <n_threads> <SF_number_density_thr>"
       )

       raise SystemExit (0)

    print ( 'snapshot:', sys.argv[1])
    print ( 'n_threads:', sys.argv[2])
    print ( 'SF_number_density_thr:', sys.argv[3])

    sb_profile_generator ( sys.argv[1], int(sys.argv[2]), float(sys.argv[3]) )

