#!/bin/bash
#
# Usage
# ./init_evol.sh <min-resolution> <max-resolution> <npref> <box size> <redshift> <gamma bkg> <heatr bg>
# ./init_evol.sh 512 4096 8 25.0 3.984 "0.594E-12 0.341E-12 0.302E-15" "0.236e-11 0.227e-11 0.925e-14"


source ../set_me.sh
source ../filename.sh


exe=./Radamesh/bin/Radamesh-1.3_64


minres=$1 # Chombo file minres
maxres=$2 # Chombo file maxres
npref=$3 # Chombo file npref
box_size=$4 # Chombo file box size in Mpc
redshift=$5 # Chombo file redshift
gamma_bg="$6" # Chombo file redshift
heatr_bg="$7" # Chombo file redshift
uvb_ssthr=$8 # Chombo file uvb self shielding threshold


input=$(chombo_file ${minres} ${maxres} ${npref})

version_id=$(radamesh_version_id ${minres} ${maxres} ${npref} "${gamma_bg}" "${heatr_bg}" ${uvb_ssthr} )
paramfile="SO.${version_id}.param"
logfile=$(init_evol_log ${minres} ${maxres} ${npref} "${gamma_bg}" "${heatr_bg}" ${uvb_ssthr} )

touch ${paramfile}
echo "" > ${paramfile}

echo "VersionID = ${version_id}" | tee -a ${paramfile}
echo "" | tee -a ${paramfile}
echo "ActualRun = .false." | tee -a ${paramfile}
echo "DensityInputFile = \"../chombo/${input}\"" | tee -a ${paramfile}
echo "" | tee -a ${paramfile}
echo "ComovingBoxSize = ${box_size}  # Mpc" | tee -a ${paramfile}
echo "" | tee -a ${paramfile}
echo "InitialRedshift = ${redshift}" | tee -a ${paramfile}
echo "FinalRedshift = ${redshift}" | tee -a ${paramfile}
echo "" | tee -a ${paramfile}
echo "Gamma_BKG = ${gamma_bg}" | tee -a ${paramfile}
echo "HeatR_BKG = ${heatr_bg}" | tee -a ${paramfile}
echo "UVB_SSThr = ${uvb_ssthr}" | tee -a ${paramfile}

cat init_evol_base.config >> ${paramfile}


echo "${exe} ${paramfile}" | tee -a ${logfile}
${exe} ${paramfile} | tee -a ${logfile}
