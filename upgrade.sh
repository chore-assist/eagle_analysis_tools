#!/bin/bash
#
# USAGE: ./upgrade.sh

git pull origin master
git submodule update --init --recursive

# Building P2C
make clean -C ./chombo/P2C/
make clean -C ./chombo/P2C/lib/amr
make -C ./chombo/P2C/lib/amr
make -C ./chombo/P2C

# Building column_density/PlotStArt
make clean -C ./column_density/PlotStArt
make -C ./column_density/PlotStArt

# Building fits_3d/PlotStArt
make clean -C ./fits_3d/PlotStArt/
make -C ./fits_3d/PlotStArt/

# Building Radamesh
make clean -C ./init_evol/Radamesh
make -C ./init_evol/Radamesh

