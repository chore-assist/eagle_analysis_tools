#!/bin/bash
#
# Usage
# ./fits_3d.sh </path/to/input/chombo> <var> <lmax>
# ./fits_3d.sh /path/to/input uniform-density 0


source ../set_me.sh
source ../filename.sh


input="$1"
var=$2
lmax=$3

exe=./PlotStArt/bin/PlotStART-0.1.0.a

output=$(fits_3d_file ${input} ${var} ${lmax})
logfile=$(fits_3d_logfile ${input} ${var} ${lmax})

echo "${exe} -inp ${input} -out ${output} -var ${var} -lmax 0 -ftype fits3D" | tee -a ${logfile}
${exe} -inp ${input} -out ${output} -var ${var} -lmax 0 -ftype fits3D | tee -a ${logfile}
